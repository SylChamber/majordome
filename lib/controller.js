/**
 * Returns a MVC controller for use with the 'view-engine' module.
 * @param {string} dirname Path of the current module. Value of '__dirname' is expected.
 * @param {Object} viewEngine View engine instance required from 'view-engine' NPM module.
 * @param {string} [viewExtension] File extension for the templates. Defaults to 'marko'.
 * @returns {Object} Controller with getView(viewName) and resolvePath(relativePath) prototype
 * methods
 * @requires module:view-engine
 */
module.exports = function(dirname, viewEngine, viewExtension) {
  'use strict';

  if (typeof dirname !== 'string') {
    throw new Error('Path "dirname" is required.');
  }

  if (viewEngine === undefined) {
    throw  new Error('viewEngine is required.');
  }

  viewExtension = viewExtension || 'marko';

  /**
   * Controller for an MVC-style web app.
   * @constructs controller
   */
  var controllerProto = {
    /**
     * Gets the view for the given view; returns a template from the view-engine module.
     * @param viewName Name of the view or template. The file extension may be omitted.
     * @returns {*} View or template from view-engine
     * @memberof controller#
     */
    getView: function getView(viewName) {
      if (viewName.lastIndexOf('.') === -1) {
        viewName = viewName + '.' + viewExtension;
      }

      var viewPath = this.resolvePath(viewName);

      return this.viewEngine.load(viewPath);
    },

    /**
     * Resolves the relative path
     * @param relativePath
     * @returns {string} Absolute path
     * @memberof controller#
     */
    resolvePath: function resolvePath(relativePath) {
      var path = require('path');
      return path.resolve(this.modulePath, relativePath);
    }
  };

  function createController() {
    return Object.create(controllerProto, {
      modulePath: {
        configurable: false,
        enumerable: true,
        writable: false,
        value: dirname
      },
      viewEngine: {
        configurable: false,
        enumerable: true,
        writable: false,
        value: viewEngine
      },
      viewExtension: {
        configurable: false,
        enumerable: true,
        writable: true,
        value: viewExtension
      }
    });
  }

  return createController();
};
