describe('Controller', function() {
  var expect = require('chai').expect;
  var controllerFactory = require('../lib/controller');

  describe('#resolvePath', function() {
    var viewEngineStub = {};

    it('returns the absolute path for a file name', function() {
      var controller = controllerFactory('/absolute/path/to', viewEngineStub);
      var resolvedPath = controller.resolvePath('somefile');

      expect(resolvedPath).to.exist.
        and.to.equal('/absolute/path/to/somefile');
    });

    it('returns the absolute path for a relative path to a file in the same folder', function() {
      var controller = controllerFactory('/absolute/path/to', viewEngineStub);
      var resolvedPath = controller.resolvePath('./somefile');

      expect(resolvedPath).to.exist.
        and.to.equal('/absolute/path/to/somefile');
    });

    it('returns the absolute path for a relative path to a file in another folder', function() {
      var controller = controllerFactory('/absolute/path/to/this', viewEngineStub);
      var resolvedPath = controller.resolvePath('../that/file');

      expect(resolvedPath).to.exist.
        and.to.equal('/absolute/path/to/that/file');
    })
  });

  describe('#getView', function() {
    var viewEngineStub = {
      load: function returnArgument(path) {
        return path;
      }
    };

    it('accepts a file name and returns the expected view', function() {
      var controller = controllerFactory('/absolute/path/to/this', viewEngineStub, 'marko');
      var view = controller.getView('view.marko');

      expect(view).to.exist.
        and.to.equal('/absolute/path/to/this/view.marko');
    });

    it('adds the extension to the file name if missing', function() {
      var controller = controllerFactory('/absolute/path/to/this', viewEngineStub, 'marko');
      var view = controller.getView('view');

      expect(view).to.exist.
        and.to.equal('/absolute/path/to/this/view.marko');
    });
  })
});
